# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-08-15 21:21
from __future__ import unicode_literals

from django.db import migrations, models

import datetime
from listings.models import Company, Price
import csv
import locale
import os
BASE = os.path.dirname(os.path.abspath(__file__))

def forwards_func(apps, schema_editor):
    #get company info
    with open(os.path.join(BASE,'top75.csv'), 'rb') as csvfile:
        fieldnames = ['Symbol', 'Name', 'LastSale', 'MarketCap', 'ADR TSO', 'IPOyear', 'Sector', 'Industry', 'Summary Quote'] 
        reader = csv.DictReader(csvfile, fieldnames)
        i = 0 
        y = []
        for row in reader:
            i += 1
            if (i - 1) == 0: continue
            c = Company(comp_sym=row['Symbol'],comp_name=row['Name'],comp_price=float(row['LastSale']), comp_cap=float(row['MarketCap']) )
            c.save()
    
    #get prices
    for f in os.listdir('stock_info/'):
    
        if f.endswith('.txt'):
            #print (os.path.join('stock_info', file))
            #print f
            
            y=[]
            y.append(f[0:-4])
            with open(os.path.join('stock_info', f)) as curr:
                for line in curr:
                    if line == '\n': continue
                    y.append(line[0:-1])
                curr.close()
            
            
            print y[0:7]
            print len(y[1:])
            for i in xrange(len(y[1:])/6):    
                d = datetime.datetime.strptime(y[i*6+1], '%m/%d/%Y').strftime('%Y/%-m/%d').replace('/', '-')
           #     print 'date: %s, open: %s, high: %s, low: %s, close: %s, volume: %s' % (d,y[i*6+2],y[i*6+3],y[i*6+4],y[i*6+5],y[i*6+6])
                p = Price(comp=Company.objects.get(comp_sym=y[0]),p_date=d,p_open=y[i*6+2],p_high=y[i*6+3],p_low=y[i*6+4],p_close=y[i*6+5],volume=y[i*6+6].replace(',',''))
                p.save()
            print y[0]

def reverse_func(apps, schema_editor):
    Company.objects.all().delete()
    Price.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]

from django.conf.urls import url

from . import views

urlpatterns = [
    #base index
    url(r'^$', views.IndexView.as_view(), name='index'),
    #company details
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    #compare details of different companies
    url(r'^compare/$', views.CompareView.as_view(), name='compare'),
    #ajax call for company info, based on timeframe
    url(r'^ajax/retrieve_comp/(?P<company_id>[0-9]+)/(?P<timeframe>[\w]+)/$', views.retrieve_comp, name='retrieve_comp'),
]

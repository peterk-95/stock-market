from django import forms
from django.forms import ModelForm, widgets
from .models import *

class GetComp(forms.Form):
    foo = forms.ModelChoiceField(queryset=Company.objects.all())
    class Meta:
        fields = ['comp_sym']

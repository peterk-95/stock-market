from django.views import generic
from listings.models import Company, Price
from .forms import GetComp

from django.core import serializers
from django.http import JsonResponse

import json
import holidays
import datetime

class IndexView(generic.ListView):
    template_name = 'listings/index.html'
    context_object_name = 'cl'

    def get_queryset(self):
        return Company.objects.order_by('comp_sym')

class DetailView(generic.DetailView):
    model = Company
    template_name = 'listings/detail.html'
    context_object_name = 'c'
    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        context['Price'] = Price.objects.filter(comp_id=self.kwargs['pk']).order_by('p_date')
        return context

class CompareView(generic.TemplateView):
    template_name = 'listings/compare.html'
    def get_context_data(self,**kwargs):
        context = super(CompareView,self).get_context_data(**kwargs)
        context['compform'] = GetComp()
        return context



def retrieve_comp(request, company_id, timeframe):
    json_serializer = serializers.get_serializer('json')()

    data = {'prices': None, 'info': None}
    tries = ['5y', '4y', '3y', '2y', '1y', '6m']
    days = [1258, 1006, 754, 503, 251, 125]
    price_count = Price.objects.filter(comp_id=company_id).count()
       
    y = None
    
    #helper method, returns only weekdays to populate dates
    def get_weekdays(length, start):    
        x = datetime.datetime.strptime(start, '%Y-%m-%d') 
        l = []
        for i in range(days[tries.index(timeframe)]-length):
            x = x - datetime.timedelta(1)
            if (x in holidays.US()) == True: x = x - datetime.timedelta(1)
            if x.weekday() == 5: x = x - datetime.timedelta(1)
            if x.weekday() == 4: x = x - datetime.timedelta(1)
            l.append(x.strftime('%Y-%m-%d'))
        l.reverse()
        return l  
    
    for d in days[tries.index(timeframe):]:
        try:
            data['prices'] = json.loads(serializers.serialize('json', Price.objects.filter(comp_id=company_id).order_by('p_date')[ price_count-d: ] ) )
            break
        except:
            pass
    
    #retrieve missing ddata
    if data['prices'] == None:
        data['prices'] = json.loads(serializers.serialize('json', Price.objects.filter(comp_id=company_id).order_by('p_date')))
        y = get_weekdays(len(data['prices']),data['prices'][0]['fields']['p_date'])
        i = 0 
        for d in y:
            ele = {u'pk': 0, u'model': u'listings.price', u'fields': {u'comp': u''+company_id, u'p_date': u''+d, u'p_close': u'0', u'volume': 0, u'p_high': u'0', u'p_low': u'0', u'p_open': u'0'}} 
            data['prices'].insert(i,ele)
            i+=1

    data['info'] = json.loads(serializers.serialize('json', Company.objects.filter(pk=company_id)))
    return JsonResponse(data)


from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
@python_2_unicode_compatible
class Company(models.Model):
    comp_name = models.CharField(max_length=200)
    comp_sym = models.CharField(max_length=10)
    comp_price = models.DecimalField(max_digits=15, decimal_places=3)
    comp_cap = models.DecimalField(max_digits=15, decimal_places=3)
    def __str__(self):
        return self.comp_name
    def symbol(self):
        return self.comp_sym


@python_2_unicode_compatible
class Price(models.Model):
    comp = models.ForeignKey(Company, on_delete=models.CASCADE)
    p_open = models.DecimalField(max_digits=14, decimal_places=2)
    p_high = models.DecimalField(max_digits=14, decimal_places=2)
    p_low = models.DecimalField(max_digits=14, decimal_places=2)
    p_close = models.DecimalField(max_digits=14, decimal_places=2)
    volume = models.IntegerField() 
    p_date = models.DateField('price date')
    def __str__(self):
        return str(self.comp.comp_sym) 
